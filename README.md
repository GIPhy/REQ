# REQ

[_REQ_](https://research.pasteur.fr/fr/tool/r%CE%B5q-assessing-branch-supports-o%C6%92-a-distance-based-phylogenetic-tree-with-the-rate-o%C6%92-elementary-quartets/) is a command line program written in [Java](https://docs.oracle.com/javase/8/docs/technotes/guides/language/index.html) that estimates the rate of elementary quartets (REQ) for each internal branch of an unrooted binary phylogenetic tree from a distance matrix, as described by [Guénoche and Garreta (2001)](https://doi.org/10.1007/3-540-45727-5_5).

## Installation

Clone this repository using the following command line:
```bash
git clone https://gitlab.pasteur.fr/GIPhy/REQ.git
```

## Compilation and execution


The source code of _REQ_ is inside the _src_ directory and can be compiled and executed in two different ways.

#### Building an executable jar file

On computers with [Oracle JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (6 or higher) installed, a Java executable jar file can be created by typing the following command lines inside the _src/_ directory:
```bash
javac REQ.java
echo Main-Class: REQ > MANIFEST.MF
jar -cmvf MANIFEST.MF REQ.jar REQ.class
rm MANIFEST.MF REQ.class
```
This will create the executable jar file `REQ.jar` that can be run with the following command line model:
```bash
java -jar REQ.jar [files]
```

#### Building a native executable

On computers with [GraalVM](hhttps://www.graalvm.org/downloads/) installed, a native executable can be built.
In a command-line window, go to the _src_ directory, and type:
```bash
javac REQ.java
native-image REQ REQ
rm -f REQ.build_artifacts.txt REQ.class
```
This will create the native executable `REQ` that can be launched with the following command line model:
```bash
./REQ [files]
```

## Usage

Run _REQ_ without option to read the following documentation:

```
 USAGE:  REQ  <dfile>  <tfile>  <outfile>  [-v]

   <dfile>    distance matrix file in either PHYLIP lower-triangular or
              square format
   <tfile>    unrooted binary phylogenetic tree file with no confidence
              value at branches in NEWICK format
   <outfile>  output file name
   -v         verbose mode
```

## Example

The directory _example_ contains two files from the study of [Garcia-Hermoso et al. (2018)](https://doi.org/10.1128/mBio.00573-18):     
* _matrix.d_: a distance matrix in PHYLIP square format estimated from 22 _Mucor circinelloides_ f. _circinelloides_ genomes,
* _tree.t_: the associated minimum-evolution phylogenetic tree in NEWICK format.

The following command line writes into the file _tree.req.t_ the phylogenetic tree from _example/tree.t_ with the rate of elementary quartets at each internal branch estimated from the distance matrix _example/matrix.d_:
```bash
REQ  example/matrix.d  example/tree.t  tree.req.t  -v
```
Because the option `-v` is set, the verbose mode will output the tree topology in NEWICK format, the list of leaf names, and, for each internal branch, the leaf quadripartition followed by the rate of elementary quartets _Re_:
```
# (((((((17,18),16),((20,21),19)),(((4,((6,7),5)),(((2,3),1),0)),(8,9))),(10,11)),(12,13)),14,15);
0: P07_621_SLS
1: 1006PHL
2: P08_701_BU2_PER
3: P01_617_BU1_SLS
4: P11_702_BU2_PER
5: E01_615_SLS
6: P04_601_BU1_SLS
7: P04_559_BU1_SLS
8: P05_600_BU1_SLS
9: P05_598_BU1_SLS
10: P04_603_BU1_SLS
11: P04_602_BU1_SLS
12: P06_032_BU1_SLS
13: P06_023_BU1_SLS
14: P02_783_BU1_SLS
15: P12_579_STR
16: P05_622_BU1_SLS
17: P10_703_BU2_PER
18: P09_704_BU2_PER
19: P03_594_BU1_SLS
20: P05_599_BU1_SLS
21: P03_592_BU1_SLS
[17][18][16][0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,19,20,21] Re=1.000 (19/19)
[17,18][16][19,20,21][0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15] Re=0.677 (65/96)
[20][21][19][0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18] Re=0.474 (9/19)
[20,21][19][16,17,18][0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15] Re=1.000 (96/96)
[16,17,18][19,20,21][0,1,2,3,4,5,6,7,8,9][10,11,12,13,14,15] Re=0.856 (462/540)
[6][7][5][0,1,2,3,4,8,9,10,11,12,13,14,15,16,17,18,19,20,21] Re=1.000 (19/19)
[6,7][5][4][0,1,2,3,8,9,10,11,12,13,14,15,16,17,18,19,20,21] Re=0.944 (34/36)
[4][5,6,7][0,1,2,3][8,9,10,11,12,13,14,15,16,17,18,19,20,21] Re=1.000 (168/168)
[2][3][1][0,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21] Re=1.000 (19/19)
[2,3][1][0][4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21] Re=1.000 (36/36)
[1,2,3][0][4,5,6,7][8,9,10,11,12,13,14,15,16,17,18,19,20,21] Re=1.000 (168/168)
[4,5,6,7][0,1,2,3][8,9][10,11,12,13,14,15,16,17,18,19,20,21] Re=1.000 (384/384)
[8][9][0,1,2,3,4,5,6,7][10,11,12,13,14,15,16,17,18,19,20,21] Re=1.000 (96/96)
[0,1,2,3,4,5,6,7][8,9][16,17,18,19,20,21][10,11,12,13,14,15] Re=0.453 (261/576)
[16,17,18,19,20,21][0,1,2,3,4,5,6,7,8,9][10,11][12,13,14,15] Re=0.488 (234/480)
[10][11][0,1,2,3,4,5,6,7,8,9,16,17,18,19,20,21][12,13,14,15] Re=1.000 (64/64)
[0,1,2,3,4,5,6,7,8,9,16,17,18,19,20,21][10,11][12,13][14,15] Re=0.594 (76/128)
[12][13][0,1,2,3,4,5,6,7,8,9,10,11,16,17,18,19,20,21][14,15] Re=1.000 (36/36)
[0,1,2,3,4,5,6,7,8,9,10,11,16,17,18,19,20,21][12,13][14][15] Re=0.500 (18/36)
output tree written into tree.req.t
```

Of note, the output file _tree.req.t_ is available inside the directory _example_.

## References

Garcia-Hermoso D, Criscuolo A, Lee SC, Legrand M, Chaouat M, Denis B, Lafaurie M, Rouveau M, Soler C, Schaal JV, Mimoun M, Mebazaa A, Heitman J, Dromer F, Brisse S, Bretagne S, Alanio A (2018) 
_Outbreak of invasive wound mucormycosis in a burn unit due to multiple strains of Mucor circinelloides f. circinelloides resolved by whole-genome sequencing_. 
**MBio**, 24;9(2):e00573-18. 
[doi:10.1128/mBio.00573-18](https://doi.org/10.1128/mBio.00573-18).

Guénoche A, Garreta H (2001) 
_Can we have confidence in a tree representation?_ 
_In_: Gascuel O, Sagot MF (eds) **Computational Biology**. Lecture Notes in Computer Science, vol 2066. Springer, Berlin, Heidelberg. 
[doi:10.1007/3-540-45727-5_5](https://doi.org/10.1007/3-540-45727-5_5). [[pdf](http://iml.univ-mrs.fr/editions/biblio/guenoche/QualiTree-1.pdf)]


## Citations

Criscuolo A (2019)
_A fast alignment-free bioinformatics procedure to infer accurate distance-based phylogenetic trees from genome assemblies_. 
**Research Ideas and Outcomes**, 5:e36178.
[doi:10.3897/rio.5.e36178](https://doi.org/10.3897/rio.5.e36178)

Shchyogolev SY, Dykman LA, Sokolov AO, Sokolov OI, Matora LY (2024)
_Quantitative intra- and intergeneric taxonomic relationships among Micrococcaceae strains reveal contradictions in the historical assignments of the strains and indicate the need for species reclassification_.
**Archives of Microbiology**, 206:165. 
[doi:10.1007/s00203-024-03896-7](https://doi.org/10.1007/s00203-024-03896-7)
