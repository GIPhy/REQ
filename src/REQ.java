/*
  ########################################################################################################

  REQ: assessing branch supports of a distance-based phylogenetic tree with rates of elementary quartets
  
  Copyright (C) 2017-2024  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

           4888888883                                                                                  
        48800007   4003 1                                                                              
     4880000007   400001 83        101  100    01   4000009  888888888 101 888888888 08    80 888888888
    4000000008    8000001 83       181  10101  01  601     1    181    181    181    08    80    181   
   40000000008    8000001 803      181  10 101 01    60003      181    181    181    08    80    181   
  100888880008    800007 60003     181  10  10101  4     109    181    181    181    68    87    181   
  81     68888    80887 600008     101  10    001   0000007     101    101    101     600009     101   
  808883     1    887  6000008                                                                         
  8000000003         480000008                                                                         
  600000000083    888000000007     10000000     40      4000009  888888888 10000000  08    80  1000000 
   60000000008    80000000007      180    39   4000    601     1    181    10        08    80  10    39
    6000000008    8000000007       18000007   47  00     60003      181    1000000   08    80  1000007 
     680000008    800000087        180       40000000  4     109    181    10        68    87  10   06 
       6888008    8000887          100      47      00  0000007     101    10000000   600009   10    00
           688    8887                                                                                 

  ########################################################################################################
*/
/*
  ########################################################################################################
  ### v2.1 (240328ac)
   + fixed bug when a leaf is not present in the distance matrix
   + no change in the quite obfuscated code style
  ### v2.0.200528ac
   + verify that the input tree is unrooted and fully binary 
   + faster running times by minimizing String conversion
  ########################################################################################################
*/

import java.io.*;
import java.util.*;
import java.text.*;

public class REQ {

    // constants
    final static String VERSION = "v2.1 (240328ac)";
    final static int MAX = 32760;
    final static int INF = Integer.MAX_VALUE;
    
    // io
    static BufferedReader in;
    static BufferedWriter out;
    static File dfile, tfile;
    static boolean verbose;

    // data
    static int n;
    static ArrayList<String> lbl;
    static float[][] dm;
    static StringBuilder nwk, tr, tro;
    static short[] tip, totip;
    static ArrayList<Double> are;

    // stuffs
    static short s, as, ae, bs, be, cs, ce;
    static int d, td, i, j, m, u, v, w, x, y, last, sup;
    static long up;
    static double re, ab, ad, bd, abcd, dn;
    static char ch;
    static String line;
    static String[] split;
    static short[] sta, stb, stc;
    static BitSet std;

    public static void main(String[] args) throws IOException {

	//### man ############################################################################################################################################################################
	if ( args.length < 3 ) {
	    System.out.println("");
	    System.out.println(" REQ " + VERSION + "           Copyright (C) 2017-2024 Institut Pasteur");
	    System.out.println("");
	    System.out.println(" >-< https://research.pasteur.fr/fr/b/_LV");
	    System.out.println(" >-< https://gitlab.pasteur.fr/GIPhy/REQ");
	    System.out.println("");
	    System.out.println(" USAGE:  REQ  <dfile>  <tfile>  <outfile>  [-v]");
	    System.out.println("");
	    System.out.println("   <dfile>    distance matrix file in either PHYLIP lower-triangular or");
	    System.out.println("              square format");
	    System.out.println("   <tfile>    unrooted binary phylogenetic tree file with no confidence");
	    System.out.println("              value at branches in NEWICK format");
	    System.out.println("   <outfile>  output file name");
	    System.out.println("   -v         verbose mode");
	    System.out.println("");
	    System.exit(0);
	}

	//### init ###########################################################################################################################################################################
	verbose = ( (args.length > 3) && args[3].equals("-v") ) ? true : false;
	if ( verbose ) System.out.println("REQ " + VERSION);
	if ( ! (dfile = new File(args[0])).exists() ) { System.err.println("distance matrix file does not exist: " + args[0]);                                        System.exit(1); }
	if ( ! (tfile = new File(args[1])).exists() ) { System.err.println("tree file does not exist: " + args[1]);                                                   System.exit(1); }
		
	//### reading distance matrix dm #####################################################################################################################################################
	in = new BufferedReader(new FileReader(dfile));
	while ( true ) try { if ( (line=in.readLine().trim()).length() != 0 ) break; } catch ( NullPointerException e ) { System.err.println("matrix file is empty"); System.exit(1); }
	try { if ( (n=Integer.parseInt(line)) > MAX ) { System.err.println("too many taxa (>" + MAX + ")");                                                           System.exit(1); } }
	catch ( NumberFormatException e ) { System.err.println("distance matrix file is incorrectly formatted: " + line);                                             System.exit(1); }
	lbl = new ArrayList<String>(n); dm = new float[n][]; i = -1;
	try {
	    while ( ++i < n ) {
		if ( (line=in.readLine().trim()).length() == 0 ) { --i; continue; } 
		split = line.split("\\s+"); lbl.add(split[j=0]); dm[i] = new float[i]; while ( ++j <= i ) dm[i][--j] = (float) Double.parseDouble(split[++j]);
	    }
	    in.close();
	} catch ( NullPointerException e ) { System.err.println("distance matrix file is incorrectly formatted");                                                     System.exit(1); }
	
	//### reading phylogenetic tree nwk ##################################################################################################################################################
	nwk = new StringBuilder(""); in = new BufferedReader(new FileReader(tfile));
	try { while ( true ) nwk = nwk.append(in.readLine().trim()); } catch ( NullPointerException e ) { in.close(); }
	tr = new StringBuilder(nwk.toString());
	
	//### discarding branch lengths from tr ##############################################################################################################################################
	u = -1 ; while ( (u=tr.indexOf(":", ++u)) != -1 ) tr = tr.delete(u, (x=((x=tr.indexOf(",",u))!=-1)?x:INF) < (y=((y=tr.indexOf(")",u))!=-1)?y:INF) ? x : y);
	
	//### encoding lbl within tr #########################################################################################################################################################
	tip = new short[n]; i = n;  //## NOTE: tip[i] = ith tax id in tr
	d = 0;                      //## NOTE: d = no. closing parentheses in tr
	u = tr.length(); v = --u;
	while ( --u >= 0 ) 
	    switch ( tr.charAt(u) ) {
	    case '(':
		if ( tr.charAt(++u) != '(' ) { tr = tr.replace(u, v, String.valueOf(j=lbl.indexOf(line=tr.substring(u, v))));
		    if ( j != -1 ) tip[--i] = (short) j; else { System.err.println(line + " does not belong to the distance matrix labels");                          System.exit(1); } }
		--u;              break;
	    case ',':
		if ( tr.charAt(++u) != '(' ) { tr = tr.replace(u, v, String.valueOf(j=lbl.indexOf(line=tr.substring(u, v))));
		    if ( j != -1 ) tip[--i] = (short) j; else { System.err.println(line + " does not belong to the distance matrix labels");                          System.exit(1); } }
		v = --u;          break; 
	    case ')': ++d; v = u; break;
	    }
	if ( n != d + 2 ) { System.err.println(( n == d + 1 ) ? "input tree is rooted" : "input tree is not binary");                                                 System.exit(1); }
       	if ( verbose ) { System.out.println("# " + tr.toString()); i = -1; while ( ++i < n ) System.out.println(i + ": " + lbl.get(i)); }

	//### estimating the rate of EQ re for each internal branch e ########################################################################################################################
	tro = new StringBuilder(tr.toString()); 
	//# first tr rooting:   ((ST1),(ST2),(ST3));  =>  (((ST1),(ST2)),(ST3));  ###############################################################
	//#                                                            |      |
	//#                                                           sup    last
	tr = tr.insert(0, '('); sup = apc(tr, tr.lastIndexOf(")")); tr = tr.insert(sup, ')'); //# NOTE: closing parenthesis at index 'sup' should not be considered for REQ calculations
	last = tr.lastIndexOf(")");
	//# NOTE: totip[x] = id in tip[] of the taxon right after position x in tr  //#     tr = "( ( 1 , 3 ) , ( 0 , 2 ) ) ;"   tip = [1,3,0,2]
	totip = new short[x=tr.length()];                                           //#  totip = [0,0,0,1,1,2,2,2,2,3,4,4,4,4]
	s = (short) n; while ( --x >= 0 ) { switch ( tr.charAt(x) ) { case '(': case ',': if ( tr.charAt(++x) != '(' ) totip[x] = --s; --x; break; } totip[x] = s; }
	
	are = new ArrayList<Double>(n); std = new BitSet(n); u = 0;
	while ( (u=tr.indexOf(")", ++u)) != last ) {
	    //# second tr rooting:   ((ST1),(ST2),(ST3));  =>  ((ST1),((ST2),(ST3)));  ##########################################################
	    //#                                                             |     |
	    //#                                                             u    last
	    if ( u == sup ) {
		last = tro.lastIndexOf(")"); tr = tro.insert(last, ')'); v = apc(tr, apc(tr, last)); tr = tr.insert(++v, '(');
		++last;            //# NOTE: closing parenthesis at index 'last' should not be considered for REQ calculations
		x = tr.length(); s = (short) n; while ( --x >= 0 ) { switch ( tr.charAt(x) ) { case '(': case ',': if ( tr.charAt(++x) != '(' ) totip[x] = --s; --x; break; } totip[x] = s; }
		sup = 0; --u; continue;
	    }
	    //# parsing every internal branch e at index u in order to obtain  lbl(STa) lbl(STb) | lbl(STc) lbl(T)-lbl(STa U STb U STc)  ########
	    //#           v     w    'u'
	    //#           |     |     |
	    //#    ( ... (((STa),(STb)),STc) ... );
	    //#          |                 |
	    //#          x                 y
	    //#          |                 |
	    //#    ( ... (STc,((STa),(STb))) ... );
	    //#               |     |     | 
	    //#               v     w    'u' 
	    v = aop(tr, u); w = apc(tr, u); x = cop(tr, u); y = ccp(tr, u);
	    as = totip[++v]; --v; ae = totip[w];                                                                //# NOTE: sta = tax list inside tr.substring(v+1, w)
	    bs = totip[++w]; be = totip[u];                                                                     //# NOTE: stb = tax list inside tr.substring(++w, u)
	    if ( ++u != y ) { cs = totip[++u]; --u; ce = totip[y]; } else { cs = totip[++x]; ce = totip[--v]; } //# NOTE: stc = tax list inside ( u+1 != y ) ? tr.substring(u+2, y) : tr.substring(++x, --v)
	    --u;
	    //if ( ae-as > be-bs ) { s = ae; ae = be; be = s; s = as; as = bs; bs = s; }
	    std.set(0, n);
	    sta = new short[(i=ae-as)]; while ( --ae >= as ) std.clear((sta[--i]=tip[ae]));
	    stb = new short[(i=be-bs)]; while ( --be >= bs ) std.clear((stb[--i]=tip[be]));
	    stc = new short[(i=ce-cs)]; while ( --ce >= cs ) std.clear((stc[--i]=tip[ce]));
	    //# comparing every quartet ab|cd with the topology induced by dm, where a,b,c,d belong to STa,STb,STc,STd, respectively  ###########
	    up = 0; d = -1;
	    while ( (d=std.nextSetBit(++d)) >= 0 ) {
		for (short a: sta) { ad = (a>d)?dm[a][d]:dm[d][a];
		    for (short b: stb) { ab = (a>b)?dm[a][b]:dm[b][a]; bd = (b>d)?dm[b][d]:dm[d][b];
			for (short c: stc) up += ( (abcd=ab+((c>d)?dm[c][d]:dm[d][c])) < ((a>c)?dm[a][c]:dm[c][a])+bd  &&  abcd < ad+((b>c)?dm[b][c]:dm[c][b]) ) ? 0b1 : 0;
		    }
		}
	    }
	    //# storing re value inside are #####################################################################################################
	    are.add(Double.valueOf(re=up/(dn=((double)(sta.length))*((double)(stb.length))*((double)(stc.length))*((double)std.cardinality()))));
	    if ( verbose ) {
		Arrays.sort(sta); Arrays.sort(stb); Arrays.sort(stc);
		System.out.println((Arrays.toString(sta) + Arrays.toString(stb) + Arrays.toString(stc) + std.toString().replace('{','[').replace('}',']')).replaceAll(" ","")
				   + String.format(" Re=%.3f", re) + " (" + ((long)up) + "/" + ((long)dn) + ")");
	    }
	}

	//### writing re values into nwk #####################################################################################################################################################
	u = 0; v = -1; while ( (u=nwk.indexOf(")", u)) != -1 ) if ( nwk.charAt(++u) != ';' ) nwk = nwk.insert(u, String.format(Locale.US, "%.3f", are.get(++v)));
	out = new BufferedWriter(new FileWriter(new File(args[2]))); out.write(nwk.toString()); out.newLine(); out.close(); 
	if ( verbose ) System.out.println("output tree written into " + args[2]);
    }

    //## containing opening parenthesis (cop): returns x from t and i, where i is a closing parenthesis index ################
    //   t = ( ... ((STa),(STb)) ... );
    //             |          |
    //             x          i
    final static int cop (final StringBuilder t, final int i) { short p = 0b1; int x = i; while ( --x >= 0 ) switch ( t.charAt(x) ) { case ')': ++p; continue; case '(': if ( --p < 0 ) return x; } return -1; }

    //## containing closing parenthesis (ccp): returns x from t and i, where i is a closing parenthesis index ################
    //   t = ( ... ((STa),(STb)) ... );
    //                  |      |
    //                  i      x
    final static int ccp (final StringBuilder t, final int i) { short p = 0; int x = i; while ( true ) switch ( t.charAt(++x) ) { case ';': return -1; case '(': ++p; continue; case ')': if ( --p < 0 ) return x; } }

    //## associated opening parenthesis (aop): returns x from t and i, where i is a closing parenthesis index ################
    //   t = ( ... ((STa),(STb)) ... );
    //             |           |
    //             x           i
    final static int aop (final StringBuilder t, final int i) { short p = 0b1; int x = i; while ( --x >= 0 ) switch ( t.charAt(x) ) { case ')': ++p; continue; case '(': if ( --p == 0 ) return x; } return -1; }

    //## associated partitionning comma (apc): returns x from t and i, where i is a closing parenthesis index ################
    //   t = ( ... ((STa),(STb)) ... );
    //                   |     |
    //                   x     i
    final static int apc (final StringBuilder t, final int i) { short p = 0; int x = i; while ( --x >= 0 ) switch ( t.charAt(x) ) { case ')': ++p; continue; case '(': --p; continue; case ',': if ( p == 0 ) return x; } return -1; }
}
    
